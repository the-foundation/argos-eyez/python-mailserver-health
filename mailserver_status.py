# -*- encoding: utf-8 -*-
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(4)

import rbl_check as rbl
import dns.resolver
from dns import reversename

global_debug = 0
### redefine this in your script

def mailserver_status(mxdomain):
    mxstatus = []
    mxdomain_state = {}
    mxdomain_state['dns']= {}
    mxdomain_state['rdns-fail']= {}
    mxdomain_state['blacklist']= {}
    #mxdomain_state['ssl']= {}
    if global_debug == 1:
        print("URL Line: ".format(mxdomain.encode('utf-8')))

###### DNS LOOKUPS 1st #################################################
    #for qtype in 'A', 'AAAA', 'MX', 'NS', 'TXT', 'SOA':
    for qtype in 'A', 'AAAA', 'TXT':
        answers = dns.resolver.query(mxdomain,qtype, raise_on_no_answer=False)    
        if answers.rrset is not None:
            pushlist=[]
            for answer in answers:
                if global_debug == 1:
                    print (answer.to_text())
                pushlist.append(answer.to_text())
            #pushlist = list(dict.fromkeys(pushlist))
            if global_debug == 1:    
                print(pushlist);
            mxdomain_state['dns'][qtype] = pushlist



########################################################################
#    rdns_matches=1
#    if global_debug == 1:    
#        print("rdns",mxdomain_state['dns'])


########## check DNS <--> PTR aka rDNS #################################
########## -> will be appended by rbl_check to each ip adress as rdns-fail  ..

#-------------IPv6--------------##


    if "AAAA" in mxdomain_state['dns']:
        pushlist=[]
        counter=0
        for x in range(len(mxdomain_state['dns']["AAAA"])):
            currentip=mxdomain_state['dns']["AAAA"][x]
            blresult=rbl.rbl_check_ip(currentip)
            cleanbl=[]
            for val in blresult:
                if val != None : 
                    if val["timeout"] == True:
                        del(val["timeout"])
                    cleanbl.append(val)
            mxdomain_state['blacklist'][str(currentip)]=cleanbl
            rev6 = dns.reversename.from_address(currentip)
            answers = dns.resolver.query(rev6, 'PTR')
            if global_debug == 1:
                print("cur: ",currentip,"reverse 6: ",rev6,"reversename",dns.reversename.to_address(rev6))
            for rdata in answers:
                if mxdomain != rdata.to_text().rstrip('.') :
                    rdns_matches=0
                    pushlist.append(mxdomain_state['dns']["AAAA"][x])
                    if global_debug ==1:
                        print('reversePTR FAIL', mxdomain_state['dns']["AAAA"][x] , " rdata: ", rdata.to_text().rstrip('.') , "for mxdomain" , mxdomain)
        if len(pushlist) > 0:
            mxdomain_state['rdns-fail']["AAAA"] = pushlist
#------------IPv4---------------##
    if "A" in mxdomain_state['dns']:
        pushlist=[]
        counter=0
        for x in range(len(mxdomain_state['dns']["A"])):
            currentip=mxdomain_state['dns']["A"][x]
            blresult=rbl.rbl_check_ip(currentip)
            cleanbl=[]
            for val in blresult:
                if val != None : 
                    if val["timeout"] == True:
                        del(val["timeout"])
                    cleanbl.append(val)
            mxdomain_state['blacklist'][str(currentip)]=cleanbl
            rev = dns.reversename.from_address(currentip)
            if global_debug == 1:
                print("cur: ",currentip,"reverse ipv6: ",rev,"reversename",dns.reversename.to_address(rev))
            answers = dns.resolver.query(rev, 'PTR')
            for rdata in answers:
                if not mxdomain.rstrip('.') == rdata.to_text().rstrip('.') :
                    rdns_matches=0
                    pushlist.append(mxdomain_state['dns']["A"][x])
                    if global_debug ==1:
                        print('reversePTR FAIL', mxdomain_state['dns']["A"][x] , " rdata: ", rdata.to_text().rstrip('.') , "for mxdomain" , mxdomain)
        if len (pushlist) > 0:
            mxdomain_state['rdns-fail']["A"] = pushlist


########################################
#        for currentip in mxdomain_state['dns']["AAAA"]:
#            print (currentip)
#            n = dns.reversename.from_address(currentip)
#            print n
#            print dns.reversename.to_address(n)
    #mxdomain_state["dns"]=[]
    del mxdomain_state["dns"]
    return mxdomain_state
#    return mxstatus
